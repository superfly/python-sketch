=============
python-sketch
=============

python-sketch is a simple Sketch file reader.

Install
-------

Install python-sketch via pip::

   pip install python-sketch


Usage
-----

Import the ``SketchFile`` class and pass it either a file-like object, a Path object or a string:

.. code:: python

   from sketch import SketchFile

   sketch = SketchFile('/path/to/file.sketch')
