import pytest

from sketch.nodes import SketchNode, camel2snake

CAMEL_CASE_PARAMS = [
    ('CamelCase', 'camel_case'),
    ('getHTTPResponse', 'get_http_response'),
    ('snake_case', 'snake_case'),
    ('_class', '_class')
]


@pytest.mark.parametrize('camel_case,snake_case', CAMEL_CASE_PARAMS)
def test_camel2snake(camel_case, snake_case):
    assert camel2snake(camel_case) == snake_case


def test_sketch_node():
    properties = {
        '_class': 'page',
        'doObjectId': '34275634',
        'layers': [
            {
                '_class': 'artboard',
                'name': 'Artboard 2',
                'layers': [
                    {
                        '_class': 'text',
                        'name': 'OK'
                    }
                ]
            }
        ],
        'style': {
            '_class': 'style',
            'do_objectID': '67F1F56F-359F-40D2-8F4D-6B6F3A69CAF1',
            'endMarkerType': 0,
            'miterLimit': 10,
            'startMarkerType': 0,
            'windingRule': 1,
            'blur': {
                '_class': 'blur',
                'isEnabled': False,
                'center': '{0.5, 0.5}',
                'motionAngle': 0,
                'radius': 10,
                'saturation': 1,
                'type': 0
            },
            'borderOptions': {
                '_class': 'borderOptions',
                'isEnabled': True,
                'dashPattern': [],
                'lineCapStyle': 0,
                'lineJoinStyle': 0
            },
            'borders': []
        }
    }

    page = SketchNode(properties)

    assert page.do_object_id == '34275634'
    assert hasattr(page, 'layers')
    assert len(page.layers) == 1
    assert isinstance(page.layers[0], SketchNode)
    assert hasattr(page.layers[0], 'layers')
    assert len(page.layers[0].layers) == 1
    assert page.layers[0].layers[0].name == 'OK'
    assert isinstance(page.style, SketchNode)
    assert isinstance(page.style.blur, SketchNode)
